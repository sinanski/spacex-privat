import React from 'react';
import styled, {keyframes} from 'styled-components';
import { RocketSVG } from "./RocketSVG";

interface Props {
  fullScreen?: boolean;
  show?: boolean;
}

const circle = keyframes`
  0% {
    transform: rotate(0);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const Container = styled.div<Props>`
  display: ${props => props.show ? 'flex' : 'none'};
  position: ${ props => props.fullScreen ? 'fixed' : 'relative' };
  top: 0;
  left: 0;
  width: ${ props => props.fullScreen ? '100vw' : '100%' };
  height: ${ props => props.fullScreen ? '100vh' : '100%' };
  justify-content: center;
  align-items: center;
`;

const Rocket = styled.div`
  width: 150px;
  height: 150px;
  animation: ${circle} 4s linear infinite;
`;

const Spinner: React.FC<Props> = (
  {
    fullScreen,
    show
  }
) => (
  <Container fullScreen={ fullScreen } show={show} >
    <Rocket>
      <RocketSVG />
    </Rocket>
  </Container>
);

export default Spinner

import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHistory, faRocket } from "@fortawesome/free-solid-svg-icons";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100vh;
  padding: 15px 6px;
  background: linear-gradient(135deg,#05f,#09f);
`;

const Button = styled.button`
  position:relative;
  border: none;
  width: 50px;
  height: 50px;
  border-radius: 6px;
  margin-bottom: 11px;
  background-color: white;
  font-size: 16px;
  color: #5a5a5a;
  p {
    font-size: 8px;
    position: absolute;
    bottom: 2px;
    left: 0;
    margin: 0;
    width: 100%;
    text-align: center;
    text-transform: uppercase;
  }
`;

const Menu = () => (
  <Container>
    <Link to={ '/launches' }>
      <Button>
        <FontAwesomeIcon
          icon={ faRocket }
          size="sm"
        />
        <p>Launches</p>
      </Button>
    </Link>
    <Link to={ '/history' }>
      <Button>
        <FontAwesomeIcon
          icon={ faHistory }
          size="sm"
        />
        <p>History</p>
      </Button>
    </Link>
  </Container>
);

export default Menu

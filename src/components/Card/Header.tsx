import React from 'react';
import { LaunchInterface } from "../../pages/Launches/LaunchConstructor";
import {
  Container,
  FlightNumber,
  Image,
  ImageContainer,
  LaunchDate,
  Manufacturer,
  MissionName,
  Nationality,
  Type,
  TypeDetails
} from "./HeaderStyles";

interface Props {
  mission_name: LaunchInterface['mission_name'];
  mission_patch_small: LaunchInterface['mission_patch_small'];
  flight_number: LaunchInterface['flight_number'];
  type: LaunchInterface['type'];
  nationality: LaunchInterface['nationality'];
  manufacturer: LaunchInterface['manufacturer'];
  date_string: LaunchInterface['date_string'];
}

const Header:React.FC<Props> = (
  {
    mission_name,
    mission_patch_small,
    flight_number,
    type,
    nationality,
    manufacturer,
    date_string
  }
  ) => (
  <Container>
    <MissionName>{ mission_name }</MissionName>
    <ImageContainer>
      { mission_patch_small &&
        <Image
          src={ mission_patch_small }
          alt={ `mission patch ${ mission_name }` }
        />
      }
      <FlightNumber smallFont={flight_number >= 100}>{ flight_number }</FlightNumber>
    </ImageContainer>
    <TypeDetails>
      <Type>{ type }</Type>
      <Nationality>{ nationality }</Nationality>
      <Manufacturer>{ manufacturer }</Manufacturer>
    </TypeDetails>
    <LaunchDate>
      <p>{ date_string }</p>
    </LaunchDate>
  </Container>
);

export default Header

import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faParachuteBox, faRocket } from "@fortawesome/free-solid-svg-icons";
import { LaunchInterface } from "../../pages/Launches/LaunchConstructor";
import * as colors from '../../constants/colors';

const Container = styled.div`
  display: flex;
  flex: 1 1 auto;
  flex-direction: column;
  padding: 0 10px 12px 10px;
`;

const Overview = styled.div`
  flex: 0 0 20px;
  font-size: 10px;
  margin-bottom: 10px;
  div {
    margin-bottom: 5px;
  }
`;

const Details = styled.div`
  display: flex;
  overflow-y: auto;
  flex: 1 1 100px;
  font-size: 0.7rem;
  line-height: 1.2rem;
`;

const Explanation = styled.span`
  opacity: .4;
`;

interface Props {
  details: LaunchInterface['details'];
  launch_success: LaunchInterface['launch_success'];
  land_success: LaunchInterface['land_success'];
  landing_intent: LaunchInterface['landing_intent'];
}

const getLandingColor = (
  land_success: Props['land_success'],
  was_intended: Props['landing_intent'],
) => {
  if ( !was_intended ) {
    return 'lightGrey'
  }

  return land_success
    ? colors.success
    : colors.error
};

const getLaunchText = (launch_success: Props['land_success']) =>
  launch_success
    ? 'successful launch'
    : 'launch failure';

const getLandingText = (
  land_success: Props['land_success'],
  was_intended: Props['landing_intent'],
) => {
  if ( !was_intended ) {
    return 'no landing intend'
  }

  return land_success
    ? 'successful landing'
    : 'landing failure'
};

const Content: React.FC<Props> = (
  {
    details,
    launch_success,
    land_success,
    landing_intent
  }
) => {
  const landingColor = getLandingColor( land_success, landing_intent );
  const launchColor = getLandingColor( launch_success, true );
  const launchText = getLaunchText( launch_success );
  const landingText = getLandingText( land_success, landing_intent );
  const style = { marginRight: '7px' }

  return (
    <Container>
      <Overview>
        <div>
          <FontAwesomeIcon
            style={ style }
            color={ launchColor }
            icon={ faRocket }
            size="sm"
          />
          <Explanation>
            { launchText }
          </Explanation>
        </div>
        <div>
          <FontAwesomeIcon
            style={ style }
            color={ landingColor }
            icon={ faParachuteBox }
            size="sm"
          />
          <Explanation>
            { landingText }
          </Explanation>
        </div>
      </Overview>
      <Details>{ details }</Details>
    </Container>
  );
}

export default Content

import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  flex: 0 0 120px;
  align-items: center;
  padding: 25px 10px;
`;

export const ImageContainer = styled.div`
  width: 100%;
  height: 35px;
  display: flex;
  align-items: center;
`;

export const Image = styled.img`
  position: relative;
  width: auto;
  height: 100%;
  z-index: 2;
`;

export const MissionName = styled.div`
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 83px;
  font-size: 13px;
  top: 3px;
`;

export const FlightNumber = styled.div<{ smallFont: boolean }>`
  position: relative;
  left: -7px;
  font-size: ${ props => props.smallFont ? '30px' : '40px' };
  opacity: 0.7;
  mix-blend-mode: soft-light;
  text-shadow: 2px 0 #04142973;
`;

export const LaunchDate = styled.div`
  position: absolute;
  left: 15px;
  bottom: 17px;
  font-size: 10px;
  opacity: 0.7;
`;

export const TypeDetails = styled.div`
  flex: 0 0 100px;
  padding: 10px;
  font-size: 11px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-end;
`;

export const Type = styled.div`
  font-size: 12px;
`;

export const Manufacturer = styled.div`
  text-align: right;
  font-size: 10px;
`;

export const Nationality = styled.div`
  opacity: 0.4;
  padding-top: 8px;
  font-size: 9px;
`;

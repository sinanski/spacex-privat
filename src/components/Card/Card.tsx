import React, { useMemo } from "react";
import styled from "styled-components";
import { LaunchData } from "../../pages/Launches/@types/LaunchData";
import { LaunchConstructor } from "../../pages/Launches/LaunchConstructor";
import boxBackground from '../../assets/img/box.svg';
import Header from "./Header";
import Content from "./Content";

interface PreviewProps {
  data: LaunchData;

  [index: string]: any;
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 200px;
  height: 300px;
  margin: 10px;
  color: white;
  background-image: url(${ boxBackground });
`;

export const Card: React.FC<PreviewProps> = ({ data }) => {
  const launch = useMemo(
    () => new LaunchConstructor( data ),
    [data]
  );

  const {
    mission_name,
    mission_patch_small,
    flight_number,
    type,
    nationality,
    manufacturer,
    date_string,
    details,
    launch_success,
    land_success,
    landing_intent
  } = launch;

  return (
    <Container>
      <Header
        mission_name={ mission_name }
        mission_patch_small={ mission_patch_small }
        flight_number={ flight_number }
        type={ type }
        nationality={ nationality }
        manufacturer={ manufacturer }
        date_string={ date_string }
      />

      <Content
        details={ details }
        launch_success={ launch_success }
        land_success={ land_success }
        landing_intent={ landing_intent }
      />
    </Container>
  );
};

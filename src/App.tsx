import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import styled from "styled-components";
import Launches from "./pages/Launches";
import History from "./pages/History";
import backgroundImage from './assets/img/background.jpg';
import Menu from "./components/Menu/Menu";

const Page = styled.div`
  display: flex;
  background-color: #111;
  background-image: url(${backgroundImage});
  background-position: center;
  background-size: cover;
  font-family: 'Roboto', sans-serif;
  max-height: 100vh;
`;

const Content = styled.div`
  display: flex;
  width: 100%;
  max-height: 100vh;
  color: white;
  padding: 20px 40px;
  overflow-y: auto;
`;

const App = () => (
  <Page>
    <Router>
      <Menu />
      <Content>
        <Switch>
          <Route exact path="/history" component={ History } />
          <Route component={ Launches } />
          <Route path="/launches" component={ Launches } />
        </Switch>
      </Content>
    </Router>
  </Page>
);

export default App;

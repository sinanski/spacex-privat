export type PossibleActions = 'history' | 'launches';

export enum Endpoints {
  history = 'FETCH_HISTORY',
  launches = 'FETCH_LAUNCHES'
}

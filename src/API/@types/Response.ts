export type LinkType = string | null;

type Links = {[index: string]: LinkType};
export type HistoryData = {
  id: number;
  title: string;
  event_date_utc: string;
  flight_number: number;
  details: string;
  links: Links;
}

export interface ApiLaunches {
  data: HistoryData[];
}


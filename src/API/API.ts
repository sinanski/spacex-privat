import axios from "axios";
import { fetch_result_limit } from "../constants/fetchConstants";

const baseUrl = 'https://api.spacexdata.com/v3/';

type Paths = 'history' | 'launches'

const fetchLaunches = async (limit = fetch_result_limit, offset?: number) =>
  fetchData('launches', limit, offset);

const fetchHistory = async (limit = fetch_result_limit, offset?: number) =>
  fetchData('history', limit, offset);

export const Api = {
  fetchLaunches,
  fetchHistory
};

const fetchData = async (
  path: Paths,
  limit: number,
  offset?: number
) => {
  const params = {
    limit,
    offset
  };
  const url = baseUrl + path;

  return axios.get( url, { params } );
};

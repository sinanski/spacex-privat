import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetch_result_limit } from "../constants/fetchConstants";
import { usePrevious } from "./usePrevious";
import { Endpoints, PossibleActions } from "../API/@types/Calls";
import { State, SubState } from "../store/saga/@types/State";

const getActionType = (action: PossibleActions) => Endpoints[action];

export const useLazyFetching = <T extends SubState>(action: T): State[T] => {
  const dispatch = useDispatch();
  const state = useSelector<State, State[T]>(
    state => state[action],
  );
  const prevState = usePrevious( state ) || [];
  const [ ready, setReady ] = useState( true );
  const [ offset, setOffset ] = useState( state.length );

  useEffect( () => {
    const fetchData = () => {
      const type = getActionType( action );
      dispatch( { type, offset } );
    };

    const updateState = () => {
      const newOffset = offset + fetch_result_limit;
      setOffset( newOffset );
      setReady( false );
    };

    if ( ready ) {
      fetchData();
      updateState();
    }
  }, [ offset, ready, action, dispatch ] );

  useEffect( () => {
    const finishedFetching = !ready && state.length !== prevState.length;

    if ( finishedFetching ) {
      setReady( true );
    }
  }, [ ready, state.length, prevState.length ] );

  return state
};

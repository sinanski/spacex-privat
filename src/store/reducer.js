const initialState = {
  launches: [],
  history: []
};

export default function reducer(state = initialState, action) {
  switch (action.type) {

    case 'STORE_LAUNCHES':
      return {
        ...state,
        launches: state.launches.concat(action.launches)
      };

    case 'STORE_HISTORY':
      return {
        ...state,
        history: state.history.concat(action.history)
      };

    default: return state
  }
}

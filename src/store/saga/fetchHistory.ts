import { call, put, takeEvery } from "@redux-saga/core/effects";
import { Api } from "../../API/API";
import { fetch_result_limit } from "../../constants/fetchConstants";
import { FetchAction } from "./@types/FetchAction";

function* fetchHistory(action: FetchAction) {
  const { offset = 0 } = action;
  try {
    const history = yield call(
      Api.fetchHistory,
      fetch_result_limit,
      offset
    );
    yield put( { type: "STORE_HISTORY", history: history.data } );
  } catch (e) {
    yield put( { type: "STORE_HISTORY_FAILED", message: e.message } );
  }
}

export function* watchFetchHistory() {
  yield takeEvery( "FETCH_HISTORY", fetchHistory );
}

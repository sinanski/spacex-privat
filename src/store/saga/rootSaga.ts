import { all } from 'redux-saga/effects'
import { watchFetchLaunches } from "./fetchLaunches";
import { watchFetchHistory } from "./fetchHistory";

export default function* rootSage() {
  yield all( [
    watchFetchLaunches(),
    watchFetchHistory()
  ] )
}

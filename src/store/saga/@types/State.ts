import { HistoryData} from "../../../API/@types/Response";
import { LaunchData } from "../../../pages/Launches/@types/LaunchData";

export enum SubState {
  history = 'history',
  launches = 'launches',
}

export interface State {
  history: HistoryData[];
  launches: LaunchData[];
}

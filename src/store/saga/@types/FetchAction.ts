export interface FetchAction {
  type: string;
  offset?: number;
}

import { call, put, takeLatest } from "@redux-saga/core/effects";
import { Api } from "../../API/API";
import { fetch_result_limit } from "../../constants/fetchConstants";
import { FetchAction } from "./@types/FetchAction";

function* fetchData(action: FetchAction) {
  const { offset = 0 } = action;
  try {
    const launches = yield call(
      Api.fetchLaunches,
      fetch_result_limit,
      offset
    );
    yield put( { type: "STORE_LAUNCHES", launches: launches.data } );
  } catch (e) {
    yield put( { type: "STORE_LAUNCHES_FAILED", message: e.message } );
  }
}

export function* watchFetchLaunches() {
  yield takeLatest( "FETCH_LAUNCHES", fetchData );
}

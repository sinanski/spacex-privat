import React from 'react';
import styled from 'styled-components';
import { useLazyFetching } from "../hooks/useLazyFetching";

const Container = styled.div`
  display: flex;
`;

const History = () => {
  //@ts-ignore
  useLazyFetching('history');

  return (
    <Container>
      History here
    </Container>
  );
};

export default History

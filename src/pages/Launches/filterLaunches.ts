import { LaunchData } from "./@types/LaunchData";
import { LaunchConstructor } from "./LaunchConstructor";

export enum FilterSettings {
  all = 'all',
  only = 'only',
  not = 'not',
}

interface FilterCallbacks {
  only(): LaunchData[],
  not(): LaunchData[],
}

interface Filters {
  launch_success: FilterSettings;
  land_success: FilterSettings;
}

export const filtered = (
  launches: LaunchData[],
  settings: Filters
) =>
  new LaunchFilter( launches, settings )
    .byLaunch()
    .byLanding()
    .result;

class LaunchFilter {
  constructor(
    private launches: LaunchData[],
    private settings: Filters,
    public result = launches
  ) {}

  byLaunch() {
    const filterLaunch = this.settings.launch_success;
    const filteredLaunchBy = {
      only: () => selectSuccessfulLaunches( this.launches ),
      not: () => selectUnsuccessfulLaunches( this.launches ),
    };
    return this.filterBy( filterLaunch, filteredLaunchBy );
  }

  byLanding() {
    const filterLand = this.settings.land_success;
    const filteredLandBy = {
      only: () => selectSuccessfulLandings( this.launches ),
      not: () => selectUnsuccessfulLandings( this.launches ),
    };
    return this.filterBy( filterLand, filteredLandBy );
  }

  private filterBy(filter: FilterSettings, filters: FilterCallbacks) {
    if ( filter === 'all' ) {
      return this;
    }

    this.result = filters[filter]();
    return this;
  }
}

const selectSuccessfulLandings = (launches: LaunchData[]) =>
  filterLaunches( launches, 'land_success', true );

const selectUnsuccessfulLandings = (launches: LaunchData[]) =>
  filterLaunches( launches, 'land_success', false );

const selectSuccessfulLaunches = (launches: LaunchData[]) =>
  filterLaunches( launches, 'launch_success', true );

const selectUnsuccessfulLaunches = (launches: LaunchData[]) =>
  filterLaunches( launches, 'launch_success', false );

const filterLaunches = (
  launches: LaunchData[],
  by: keyof Filters,
  success: boolean
) =>
  launches.filter( single => {
    const launch = new LaunchConstructor( single );
    return success
      ? launch[by]
      : !launch[by];
  } );

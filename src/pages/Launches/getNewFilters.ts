import { FilterSettings } from "./filterLaunches";
import {FilterInterface} from "./@types/Filters";
const filterSettings = Object.keys( FilterSettings );

const settings = [
  FilterSettings.all,
  FilterSettings.only,
  FilterSettings.not,
];

const updateIndex = (
  filters: FilterInterface,
  filter: keyof FilterInterface
): number => {
  const index = filterSettings.indexOf( filters[filter] );
  return index >= filterSettings.length - 1
    ? 0
    : index + 1
};
export const getNewFilters = (
  filters: FilterInterface,
  filter: keyof FilterInterface
) => {
  const index = updateIndex( filters, filter );
  return {
    ...filters,
    [filter]: settings[index]
  }
};

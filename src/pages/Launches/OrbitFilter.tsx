import React from 'react';
import styled from 'styled-components';
import worldImage from '../../assets/img/world.png';

const Container = styled.div`
  position: absolute;
  width: 230px;
  height: 230px;
  bottom: -80px;
  right: -80px;
  img {
    height: 200px;
    width: 200px;
    position: absolute;
    bottom: 0;
    right: 0;
  }
`;

const Orbit = styled.div<{ distance: number }>`
  position: absolute;
  width: ${ props => props.distance * 2 + 200 + 'px' };
  height: ${ props => props.distance * 2 + 200 + 'px' };
  border: 1px solid white;
  border-radius: 50%;
  bottom: ${ props => props.distance * -1 + 'px' };
  right: ${ props => props.distance * -1 + 'px' };
  opacity: .3;
  transition: opacity 2.5s ease;
  z-index: ${ props => 100 - props.distance };
  &:hover {
    opacity: 1;
    &:before {
      opacity: .2;
    }
    &:after {
      opacity: .4;
    }
  }
  &:before {
    content: '';
    opacity: 0;
    position: absolute;
    right: -6px;
    bottom: -6px;
    display: block;
    width: calc(100% + 12px);
    height: calc(100% + 12px);
    border-radius: 50%;
    border: 12px solid #62b5ff9c;
    z-index: 1;
  }
  &:after {
    content: '';
    opacity: 0;
    position: absolute;
    right: -2px;
    bottom: -2px;
    display: block;
    width: calc(100% + 4px);
    height: calc(100% + 4px);
    border-radius: 50%;
    border: 4px solid #62b5ff9c;
    z-index: 1;
  }
`;

const onClick = (distance: string): void => {
  alert( `I should trigger filter for ${ distance } orbit` )
};

const OrbitFilter = () => (
  <Container>
    <Orbit distance={ 8 } onClick={ () => onClick( 'closest' ) } />
    <Orbit distance={ 20 } onClick={ () => onClick( 'medium' ) } />
    <Orbit distance={ 34 } onClick={ () => onClick( 'furthest' ) } />
    <img src={ worldImage } alt="world" />
  </Container>
);

export default OrbitFilter

import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faParachuteBox, faRocket } from "@fortawesome/free-solid-svg-icons";
import OrbitFilter from "./OrbitFilter";
import { FilterInterface } from "./@types/Filters";
import * as colors from '../../constants/colors';

interface Props {
  onFilterLaunches(): void;

  onFilterLandings(): void;

  activeFilters: FilterInterface
}

const Container = styled.div`
  position: absolute;
  display: flex;
  bottom: 0;
  right: 0;
  width: 160px;
  height: 100%;
  overflow: hidden;
  pointer-events: none;
  justify-content: flex-end;
  padding: 10px;
  * {
    pointer-events: all;
  }
`;

const Buttons = styled.div`
  display: flex;
  flex-direction: column;
  width: 50px;
`;

const FilterButton = styled.button`
  width: 40px;
  height: 40px;
`;

const filterColors = {
  all: colors.gray,
  only: colors.success,
  not: colors.error,
};

const Filters: React.FC<Props> = (
  {
    onFilterLaunches,
    onFilterLandings,
    activeFilters
  }
) => {
  const launchColor = filterColors[activeFilters.launch_success];
  const landColor = filterColors[activeFilters.land_success];

  return (
    <Container>
      <Buttons>
        Filter by
        <FilterButton onClick={ onFilterLaunches }>
          <FontAwesomeIcon
            icon={ faRocket }
            color={ launchColor }
            size="sm"
          />
        </FilterButton>
        <FilterButton onClick={ onFilterLandings }>
          <FontAwesomeIcon
            icon={ faParachuteBox }
            color={ landColor }
            size="sm"
          />
        </FilterButton>
      </Buttons>
      <OrbitFilter />
    </Container>
  );
}

export default Filters

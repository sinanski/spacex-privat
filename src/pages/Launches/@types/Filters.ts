import { FilterSettings } from "../filterLaunches";

export interface FilterInterface {
  launch_success: FilterSettings;
  land_success: FilterSettings;
}

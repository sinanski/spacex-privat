import { LinkType } from "../../../API/@types/Response";

type Rocket = {
  "rocket_id": string;
  "rocket_name": string;
  "rocket_type": string;
  "first_stage": {
    "cores": Cores[];
  };
  "second_stage": {
    "payloads": Payloads[];
  };
  "fairings": {
    "recovery_attempt": boolean;
    "recovered": boolean;
  };
}

export type Cores = {
  "land_success": boolean | null;
  "landing_intent": boolean | null;
}

export type Payloads = {
  "nationality": string;
  "manufacturer": string;
  "payload_type": string;
  "payload_mass_kg": number;
  "orbit": string;
}
export type LaunchLinkList = {
  "mission_patch": string;
  "mission_patch_small": string;
  "reddit_campaign": LinkType;
  "reddit_launch": LinkType;
  "reddit_recovery": LinkType;
  "reddit_media": LinkType;
  "presskit": LinkType;
  "article_link": LinkType;
  "wikipedia": LinkType;
  "video_link": LinkType;
  "flickr_images": string[];
}

export interface LaunchData {
  "flight_number": number;
  "mission_name": string;
  "upcoming": boolean;
  "launch_date_local": string;
  "rocket": Rocket;
  "launch_site": {
    "site_name": string;
    "site_name_long": string;
  },
  "launch_success": boolean;
  "links": LaunchLinkList;
  "details": "Engine failure at 33 seconds and loss of vehicle",
}
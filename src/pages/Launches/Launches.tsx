import React, { useMemo, useState } from 'react';
import styled from 'styled-components';
import { useLazyFetching } from "../../hooks/useLazyFetching";
import Card from "../../components/Card";
import { LaunchData } from "./@types/LaunchData";
import { filtered, FilterSettings } from "./filterLaunches";
import Filters from "./Filters";
import { getNewFilters } from "./getNewFilters";
import { FilterInterface } from "./@types/Filters";
import Spinner from "../../components/Spinner";

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const initialFilters: FilterInterface = {
  launch_success: FilterSettings.all,
  land_success: FilterSettings.all,
};

const mapLaunches = (launches: LaunchData[], filters: FilterInterface) =>
  filtered( launches, filters )
    .map( (launch, i) => (
      <Card
        key={ i }
        data={ launch }
      />
    ) );

const Launches = () => {
  // @ts-ignore
  const launches: LaunchData[] = useLazyFetching<'launches'>( 'launches' );
  const [ filters, setFilters ] = useState<FilterInterface>( initialFilters );

  const updateFilter = (filter: keyof FilterInterface) => {
    const newFilter = getNewFilters( filters, filter );
    setFilters( newFilter );
  };

  const onFilterLaunches = () => {
    updateFilter( 'launch_success' )
  };

  const onFilterLandings = () => {
    updateFilter( 'land_success' )
  };

  const Cards = useMemo(
    () => mapLaunches( launches, filters ),
    [ launches, filters ]
  );

  return (
    <Container>
      <Filters
        onFilterLaunches={ onFilterLaunches }
        onFilterLandings={ onFilterLandings }
        activeFilters={ filters }
      />
      { Cards }
      <Spinner fullScreen show={!launches.length} />
    </Container>
  );
};

export default Launches

import _ from 'lodash';
import { Cores, LaunchData, Payloads } from "./@types/LaunchData";

const getMonthName = (index: number) => {
  const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  return monthNames[index]
};


export class LaunchConstructor {

  constructor(
    private data: LaunchData,
    private date = new Date(data.launch_date_local),
    private payloads: Payloads = _.get(data.rocket, 'second_stage.payloads[0]', {}),
    private cores: Cores = _.get(data.rocket, 'first_stage.cores[0]', {}),
    public mission_name = data.mission_name,
    public flight_number = data.flight_number,
    public details = data.details,
    public launch_success = data.launch_success,
    public land_success = cores.land_success,
    public landing_intent = cores.landing_intent,
    public mission_patch_small = _.get(data.links, 'mission_patch_small'),
    public type: Payloads['payload_type'] = payloads.payload_type,
    public nationality: Payloads['nationality'] = payloads.nationality,
    public manufacturer: Payloads['manufacturer'] = payloads.manufacturer,
    public month = getMonthName(date.getMonth()),
    public year = date.getFullYear(),
    public date_string = `${month} ${year}`,
  ) {

  }
}

export interface LaunchProps extends Partial<LaunchConstructor> {}
export interface LaunchInterface extends LaunchConstructor {}
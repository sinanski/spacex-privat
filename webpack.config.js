const path = require( 'path' );
module.exports = {
  entry: './src/index.tsx',
  output: {
    path: path.resolve( 'dist' ),
    filename: 'main.js'
  },
  module: {
    rules: [
      { test: /\.js$|jsx|ts$|tsx/, loader: 'babel-loader', exclude: /node_modules/ }
    ]
  }
};

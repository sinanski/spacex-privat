# README #

##General Stuff
I don't know how much time you expect your candidates to invest in this task but I would have needed much more to fulfil all AKs in a clean setup.

Since I've had 3-4 evenings I decided to create a subset that let's you see how I code and also some ideas for a UX (didn't have time for a final design but it is ok for the time being).

Wanted to build a modal for details but had no time.
Would have liked to refactor `Card` to also manage history data but again - no time.
Had the idea for the nice looking orbital filter. Implemented the UI part but no functionality :)

##Functionality
The router is set up and routes to `launches` and `history`.
Launches is working, history is just a placeholder.
The simple version of the `useLazyFetching()` simulates an endless scrolling / fetching behaviour.


##Thoughts

#### useLazyApi()
Since the API response was very slow I decided to build a lazy API that fetches only a payload of elements.

But since I am implementing filters I am fetching all results over time. This way the users sees progress even in filtered lists.

With more time I would have implemented an endless scrolling and smarter fetches for the selected information.


##Known Issues
Since I am quite new to TypeScript (3 month of actual working with it) some stuff did not work as intended.
This is the reason for the `@ts-ignore` in `Launches`
